const cToF = (cTemp) => {
    const cToFahr = (cTemp * 9) / 5 + 32;
    const message = cTemp + '\xB0C is equal to ' + cToFahr.toFixed(2) + '\xB0F.';
    console.log(message);
    return message;
};

const fToC = (fTemp) => {
    const fToCel = ((fTemp - 32) * 5) / 9;
    const message = fTemp + '\xB0F is equal to ' + fToCel.toFixed(2) + '\xB0C.';
    console.log(message);
    return message;
};


module.exports = {
    cToF,
    fToC
};