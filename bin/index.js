#!/usr/bin/env node

const program = require('commander');
const {cToF, fToC} = require ('../lib/convert');
program.version('0.0.1')


program
  .command('cToF')
  .description('Convent from Celsius to Fahrenheit ')
  .action((a) => cToF(a));
program
  .command('fToC')
  .description('Convent from Fahrenheit to  Celsius')
  .action((a) => fToC(a));

program.parse(process.argv);